<?php 

class Validator {

  private $data;
  private $errors = [];
  private static $fields = ['sku', 'name', 'price', 'productType', 'size', 'weight', 'height', 'width', 'length'];

  public function __construct($post_data){
    $this->data = $post_data;
  }

  public function validateForm(){
    foreach(self::$fields as $field){
      if(!array_key_exists($field, $this->data)){
        trigger_error("'$field' is not present in the data");
        return;
      }
    }

    $this->validateSku();
    $this->validateName();
    $this->validatePrice();
    $this->validateType();
    $this->validateSize();
    $this->validateWeight();
    $this->validateHeight();
    $this->validateWidth();
    $this->validateLength();
    return $this->errors;

  }
     
  private function validateSku(){

    $val = trim($this->data['sku']);

    if(empty($val)){
      $this->addError('sku', 'sku cannot be empty');
    } else {
      if(!preg_match('/^-?\d*(\.\d+)?$/ ', $val)){
        $this->addError('sku','sku must be number');
      }
    }

  }

  private function validateName(){

    $val = trim($this->data['name']);

    if(empty($val)){
      $this->addError('name', 'name cannot be empty');
    }
  }

  private function validatePrice(){

    $val = trim($this->data['price']);

    if(empty($val)){
      $this->addError('price', 'price cannot be empty');
    } else {
      if(!preg_match('/^-?\d*(\.\d+)?$/ ', $val)){
        $this->addError('price','price must be number');
      }
    }
  }

  private function validateType(){

    $val = trim($this->data['productType']);

    if(empty($val)){
      $this->addError('productType', 'product type cannot be empty');
    } 
  }
  
  private function validateSize(){

    $typeVal = trim($this->data['productType']);
    $val = trim($this->data['size']);
    if($typeVal == 'DVD'){
      if(empty($val)){
        $this->addError('size', 'size cannot be empty');
      } else {
        if(!preg_match('/^-?\d*(\.\d+)?$/ ', $val)){
          $this->addError('size','size must be number');
        }
      }
    }
  
  }

  private function validateWeight(){
    $typeVal = trim($this->data['productType']);
    $val = trim($this->data['weight']);
    if($typeVal == 'Book'){
      if(empty($val)){
        $this->addError('weight', 'weight cannot be empty');
      } else {
        if(!preg_match('/^-?\d*(\.\d+)?$/ ', $val)){
          $this->addError('weight','weight must be number');
        }
      }
    }
  
  }

  private function validateHeight(){

    $typeVal = trim($this->data['productType']);
    $val = trim($this->data['height']);
    if($typeVal == 'Furniture'){
      if(empty($val)){
        $this->addError('height', 'height cannot be empty');
      } else {
        if(!preg_match('/^-?\d*(\.\d+)?$/ ', $val)){
          $this->addError('height','height must be number');
        }
      }
    }

  }

  private function validateWidth(){

    $typeVal = trim($this->data['productType']);
    $val = trim($this->data['width']);
    if($typeVal == 'Furniture'){
      if(empty($val)){
        $this->addError('width', 'width cannot be empty');
      } else {
        if(!preg_match('/^-?\d*(\.\d+)?$/ ', $val)){
          $this->addError('width','width must be number');
        }
      }
    }


  }
  
  private function validateLength(){

    $typeVal = trim($this->data['productType']);
    $val = trim($this->data['length']);
    if($typeVal == 'Furniture'){
      if(empty($val)){
        $this->addError('length', 'length cannot be empty');
      } else {
        if(!preg_match('/^-?\d*(\.\d+)?$/ ', $val)){
          $this->addError('length','length must be number');
        }
      }
    }

  }

  private function addError($key, $val){
    $this->errors[$key] = $val;
  }

}