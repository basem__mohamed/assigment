<?php 

class Book extends Product {
    private $weight;

    public function __construct($sku, $name, $price, $productType, $size, $height, $width, $length, $weight){
        parent::__construct($sku, $name, $price, $productType);
        $this->weight = $weight;
    }

    public function parentProps(){
    return $this->getProps();
    }

    public function addProduct() {
    $object = json_decode(json_encode($this->parentProps()));
    $object->weight = $this->weight;
    $sql = "INSERT INTO products(sku, name, price, productType, weight) VALUES (?, ?, ?, ?, NULLIF(?, ''))";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$object->sku, $object->name, $object->price, $object->productType, $object->weight]);
    }
    

    
}