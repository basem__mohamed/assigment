<?php 

 class Product extends dbConnection {

  private $sku;
  private $name;
  private $price;
  private $productType;



  public function __construct($sku, $name, $price, $productType){
    $this->sku = $sku;
    $this->name = $name;
    $this->price = $price;
    $this->productType = $productType;
  }

  public function getProps(){
    return ['sku' => $this->sku, 'name' => $this->name, 'price' => $this->price, 'productType' => $this->productType];
  }

  public function getProducts() {
    $sql = "SELECT * FROM products";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute();
    while($result = $stmt->fetchAll()) {
      return $result;
    };
  }


  public function delProducts($id) {
    $sql = "DELETE FROM products WHERE id IN($id)";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$id]);
  }
}