<?php 

class DVD extends Product {

  private $size;

  public function __construct($sku, $name, $price, $productType, $size){
    parent::__construct($sku, $name, $price, $productType);
    $this->size = $size;
  }

  public function parentProps(){
    return $this->getProps();
  }
  public function addProduct() {
    $object = json_decode(json_encode($this->parentProps()));
    $object->size = $this->size;
    $sql = "INSERT INTO products(sku, name, price, productType, size) VALUES (?, ?, ?, ?, NULLIF(?, ''))";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$object->sku, $object->name, $object->price, $object->productType, $object->size]);
  }

}