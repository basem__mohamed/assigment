<?php 

class Furniture extends Product {

    private $height;
    private $width;
    private $length;

    public function __construct($sku, $name, $price, $productType, $size, $height, $width, $length) {
        parent::__construct($sku, $name, $price, $productType);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    public function parentProps(){
    return $this->getProps();
    }
    

  public function addProduct() {
    $object = json_decode(json_encode($this->parentProps()));
    $object->height = $this->height;
    $object->width = $this->width;
    $object->length = $this->length;
    $sql = "INSERT INTO products(sku, name, price, productType, height, width, length) VALUES (?, ?, ?, ?, NULLIF(?, ''), NULLIF(?, ''), NULLIF(?, ''))";
    $stmt = $this->connect()->prepare($sql);
    $stmt->execute([$object->sku, $object->name, $object->price, $object->productType, $object->height, $object->width, $object->length]);
  }
    
}