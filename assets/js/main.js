$(document).ready(function () {

  $(".DVD").show();
  $(".Book").hide();
  $(".Furniture").hide();
  $("#productType").on("change", function () {
    if (this.value == "DVD") {
      $(".DVD").show();
      $(".Book").hide();
      $(".Furniture").hide();

      $("#size").attr("required", true);
      $("#weight").removeAttr("required");
      $(".dimension").removeAttr("required");
    }
    if (this.value == "Book") {
      $(".DVD").hide();
      $(".Book").show();
      $(".Furniture").hide();

      $("#weight").attr("required", true);
      $("#size").removeAttr("required");
      $(".dimension").removeAttr("required");
    }
    if (this.value == "Furniture") {
      $(".DVD").hide();
      $(".Book").hide();
      $(".Furniture").show();

      $(".dimension").attr("required", true);
      $("#size").removeAttr("required");
      $("#weight").removeAttr("required");
    }
  });

  $("#product_form").submit(function (event) {
    event.preventDefault();

    var array = $(this).serializeArray();
    var data = {};
    $.map(array, function (n, i) {
      data[n["name"]] = n["value"];
    });
    var request = {
      url: `product.service.php`,
      method: "POST",
      data: array,
    };

    $.ajax(request).done(function (response) {
      let result = JSON.parse(response);
      console.log(result);
      if(result.success){
        location.assign('/test/index.php')
      }else{
        for (const [key, value] of Object.entries(result.failed)) {
          $(`.${key}`).html(value);
        }
      }
    });
  });
});
