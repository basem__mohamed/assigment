<?php 
  include "./includes/class-autoload.inc.php";

  if(isset($_POST['name'])) {
    
      // validate entries
      $errors = [];
      $validator = new Validator($_POST);
      $errors = $validator->validateForm();
      // if errors is empty --> save data to db
      if(!$errors){
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $productType = $_POST['productType'];
        $size = $_POST['size'];
        $weight = $_POST['weight'];
        $height = $_POST['height'];
        $width = $_POST['width'];
        $length = $_POST['length'];
        $product = new Product($sku, $name, $price, $productType);
        $Type = new $productType($sku, $name, $price, $productType, $size, $height, $width, $length, $weight);
        $Type->addProduct();
        echo json_encode(['code'=>200, 'success'=>'success']);
      }else{
        // return $errors;
        echo json_encode(['code'=>400, 'failed'=>'failed']);
      }

  } else if(isset($_POST['MASS_DELETE'])) {
    $product = new Product('','','','');
    $ids = $_POST['delete-checkbox'];
    $id = implode(',',$ids);
    $product->delProducts($id);

    header("location: {$_SERVER['HTTP_ORIGIN']}/test/index.php");
  }