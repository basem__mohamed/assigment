<?php
  require_once('./templates/header.php');
?>
<div class="container">
  <main>
    <div class="py-5 text-center">
      <h2 class="fw-light">Add New Product</h2>
    </div>

    <div class="row g-5 container">
      <div class="col-12">
        <form id="product_form">
          <div class="row g-3">
            <div class="col-sm-12">
              <label for="sku" class="fw-light form-label">SKU</label>
              <input type="number" step=0.01 class="form-control" name="sku" id="sku" required>
              <small class="fw-light text-danger sku"></small>
            </div>

            <div class="col-sm-12">
              <label for="name" class="fw-light form-label">Name</label>
              <input type="text" class="form-control" name="name" id="name" required>
              <small class="fw-light text-danger name"></small>
            </div>

            <div class="col-sm-12">
              <label for="price" class="fw-light form-label">Price ($)</label>
              <input type="number" step=0.01 class="form-control" name="price" id="price" required>
              <small class="fw-light text-danger price"></small>
            </div>


            <div class="col-md-12">
              <label for="productType" class="fw-light form-label">Type</label>
              <select class="form-select" id="productType" name="productType" required>
                <option value="DVD" id="DVD">DVD</option>
                <option value="Furniture" id="Furniture">Furniture</option>
                <option value="Book" id="Book">Book</option>
              </select>
              <small class="fw-light text-danger productType"></small>
            </div>

            <div class="col-sm-12 DVD">
              <label for="size" class="fw-light form-label">Size (MB)</label>
              <input type="number" step=0.01 class="form-control" name="size" id="size" required>
              <small id="size" class="form-text text-muted">Please, provide size in MB</small>
              <small class="fw-light text-danger size"></small>
            </div>

            <div class="col-sm-12 Book">
              <label for="weight" class="fw-light form-label">Weight (KG)</label>
              <input type="number" step=0.01 class="form-control" name="weight" id="weight">
              <small id="weight" class="form-text text-muted">Please, provide weight in KG</small>
              <small class="fw-light text-danger weight"></small>
            </div>

            <div class="col-sm-12 Furniture" style="">
              <label for="height" class="fw-light form-label">Height (CM)</label>
              <input type="number" step=0.01 class="form-control dimension" name="height" id="height">
              <small id="weight" class="form-text text-muted">Please, provide height dimension in CM</small>
              <small class="fw-light text-danger height"></small>
            </div>

            <div class="col-sm-12 Furniture">
              <label for="width" class="fw-light form-label">Width (CM)</label>
              <input type="number" step=0.01 class="form-control dimension" name="width" id="width">
              <small id="weight" class="form-text text-muted">Please, provide width dimension in CM</small>
              <small class="fw-light text-danger width"></small>
            </div>

            <div class="col-sm-12 Furniture">
              <label for="length" class="fw-light form-label">Length (CM)</label>
              <input type="number" step=0.01 class="form-control dimension" name="length" id="length">
              <small id="weight" class="form-text text-muted">Please, provide length dimension in CM</small>
              <small class="fw-light text-danger length"></small>
            </div>

          </div>
          <div class="d-flex justify-content-around">
            <button class="col-6 m-2 fw-light btn btn-outline-primary btn-lg mt-5 mb-5" name="submit" type="submit">Save</button>
            <a href="/test/index.php" class="col-6 m-2 fw-light btn btn-outline-danger btn-lg mt-5 mb-5">Cancel</a>
          </div>
        </form>
      </div>
    </div>
  </main>
  <?php
  require_once('./templates/footer.php')
?>