<?php
  include "./includes/class-autoload.inc.php";
  require_once('./templates/header.php');
?>
<main>
  <form action="product.service.php" method="POST">
    <section class="py-5 text-center container">
      <div class="row py-lg-5 d-flex justify-content-between">
        <div class="row col-6">
          <h1 class="fw-light">Product List</h1>
        </div>
        <div class="row col-lg-6 col-md-8 mx-auto">
          <p>
          <a href="/test/create.php" class="fw-light btn btn-outline-primary my-2">ADD</a>
            <button class="fw-light btn btn-outline-danger my-2" id="delete-product-btn" name="MASS_DELETE"
              type="submit">MASS DELETE</button>
          </p>
        </div>
      </div>
    </section>
    <div class="album py-5 bg-light">
      <div class="container">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
          <?php  $products = new Product('', '', '', ''); ?>
          <?php  if($products->getProducts()) : ?>
          <?php  foreach($products->getProducts() as $product) : ?>
          <div class="col">
            <div class="card shadow-sm">
              <div class="card-body">
                <div class="d-flex justify-content-between align-items-center mb-2">
                  <div class="form-check">
                    <input class="form-check-input delete-checkbox" name="delete-checkbox[]" type="checkbox"
                      value="<?= $product['id'] ?>">
                  </div>
                </div>
                <div class="text-center">
                  <p class="fw-light">SKU: <?= $product['sku'] ?></p>
                  <p class="fw-light">Name: <?= $product['name'] ?></p>
                  <p class="fw-light">Price: <?= $product['price'] ?> $</p>
                  <p class="fw-light">Type: <?= $product['productType'] ?></p>
                  <?php  if($product['productType'] == 'DVD') : ?>
                  <p class="fw-light">Weight: <?= $product['size'] ?> (MB)</p>
                  <?php endif; ?>
                  <?php  if($product['productType'] == 'Furniture') : ?>
                  <p class="fw-light">Dimensions: <?= $product['height'] ?> * <?= $product['width'] ?> *
                    <?= $product['length'] ?> (CM)</p>
                  <?php endif; ?>
                  <?php  if($product['productType'] == 'Book') : ?>
                  <p class="fw-light">Size: <?= $product['weight'] ?> (KG)</p>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </form>
</main>
<?php
  require_once('./templates/footer.php')
?>